import os
from PIL import Image
from torchvision import transforms
import torch.utils.data


def image_loader(image_path):
    image = Image.open(image_path).convert('RGB')
    resize = transforms.Resize((32,32))
    tensor = transforms.ToTensor()

    image = resize(image)
    image = tensor(image)

    return image


class ImageLoader(torch.utils.data.Dataset):
    def __init__(self, file_path, loader = image_loader):
        self.file_path = file_path
        self.loader = loader
        self.data = list()
        self.labels = list()
        # self.category_map = {'0':  0, '1':  1, '2':  2, '3':  3, '4':  4, '5':  5, '6':  6, '7':  7, '8':  8, '9':  9,
        #                      'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19,
        #                      'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24, 'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29,
        #                      'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35, 'A': 36, 'B': 37, 'C': 38, 'D': 39,
        #                      'E': 40, 'F': 41, 'G': 42, 'H': 43, 'I': 44, 'J': 45, 'K': 46, 'L': 47, 'M': 48, 'N': 49,
        #                      'O': 50, 'P': 51, 'Q': 52, 'R': 53, 'S': 54, 'T': 55, 'U': 56, 'V': 57, 'W': 58, 'X': 59,
        #                      'Y': 60, 'Z': 61
        #                     }
        self.category_map = {'0':  0, '1':  1, '2':  2, '3':  3, '4':  4, '5':  5, '6':  6, '7':  7, '8':  8, '9':  9,
                             'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15, 'G': 16, 'H': 17, 'I': 18, 'J': 19,
                             'K': 20, 'L': 21, 'M': 22, 'N': 23, 'O': 24, 'P': 25, 'Q': 26, 'R': 27, 'S': 28, 'T': 29,
                             'U': 30, 'V': 31, 'W': 32, 'X': 33, 'Y': 34, 'Z': 35
                            }

        for folder in os.listdir(self.file_path):
            for image in os.listdir(os.path.join(self.file_path, folder)):
                image_path = os.path.join(self.file_path, folder, image)
                self.data.append(image_path)
                self.labels.append(self.category_map[folder])

    def __getitem__(self, index):
        image_path = self.data[index]
        label = self.labels[index]
        image = self.loader(image_path)
        return image, label

    def __len__(self):
        return len(self.data)