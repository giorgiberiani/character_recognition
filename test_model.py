from PIL import Image
import numpy
from torchvision import transforms
import torch
import torch.cuda as cuda
from resnet import resnet18

def preproces_iamge(image_path):
    image = Image.open(image_path).convert('RGB')
    resize = transforms.Resize((32,32))
    tensor = transforms.ToTensor()

    image = resize(image)
    image = tensor(image).unsqueeze(0)

    return image

def get_letter_from_index(index):

    # category_map = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    #                 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
    #                 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    #                  'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
    #                 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    #                 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    #                 'Y', 'Z'
    #                 ]

    category_map = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                    'U', 'V', 'W', 'X', 'Y', 'Z'
                    ]
    return category_map[index]


def predict(image_path):
    image = preproces_iamge(image_path)

    net = resnet18(pretrained=True)
    net.load_state_dict(torch.load('weights/characer-recognition.pth'))
    with torch.no_grad():
        net.eval()
        if cuda.is_available():
            net = net.cuda()
        output = net(image)
        output = numpy.array(output)
        index = numpy.argmax(output)

        predicted = get_letter_from_index(index)
        print(predicted)
        return predicted



if __name__ == "__main__":
    predict('/home/beriani/Downloads/88.png')