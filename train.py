import torch.nn as nn
import torch.cuda as cuda
import torch.utils.data
from dataloader import ImageLoader
from resnet import resnet18

def train(train_dataet, test_dataset, batch_size = 1024, number_of_epoches = 100, lerning_rate = 0.001):
    train_data = ImageLoader(train_dataet)
    test_data = ImageLoader(test_dataset)

    train_dataloader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=1)
    test_dataloader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True, num_workers=1)

    net = resnet18(pretrained=True)
    if cuda.is_available():
        net = net.cuda()

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=lerning_rate, momentum=0.9)

    train_loss = []
    valid_loss = []
    train_accuracy = []
    valid_accuracy = []
    for epoch in range(number_of_epoches):
        iter_loss = 0.0
        correct = 0
        iterations = 0
        net.train()
        for i , (items, classes) in enumerate(train_dataloader):
            if cuda.is_available():
                items = items.cuda()
                classes = classes.cuda()

            optimizer.zero_grad()
            outputs = net(items)
            iter_loss = criterion(outputs, classes)
            iter_loss.backward()
            optimizer.step()

            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == classes.data).sum()
            iterations += 1

        train_loss.append(iter_loss / iterations)
        train_accuracy.append((100 * correct / len(train_dataloader.dataset)))

        loss = 0.0
        correct = 0
        iterations = 0
        net.eval()
        if cuda.is_available():
            net = net.cuda()

        for i, (items, classes) in enumerate(test_dataloader):

            if cuda.is_available():
                items = items.cuda()
                classes = classes.cuda()

            outputs = net(items)
            loss += criterion(outputs, classes).item()

            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == classes.data).sum()

            iterations += 1

        valid_loss.append(loss / iterations)
        valid_accuracy.append((100 * correct / len(test_dataloader.dataset)))

        results = f'Epoch {epoch+1}/{number_of_epoches} ' \
                 f'Tr Loss: {train_loss[-1]}, '\
                 f'Tr Acc: {train_accuracy[-1]}, '\
                 f'Val Loss: {valid_loss[-1]}, '\
                 f'Val Acc: {valid_accuracy[-1]}'\

        print(results)
        with open('weights/train_test_results.txt','a') as txt:
            txt.write(results +'\n')
        if epoch > 96:
            torch.save(net.state_dict(), f'weights/epoch_{epoch+1}.pth')


if __name__ == '__main__':
    train('dataset/train', 'dataset/test', 1024)